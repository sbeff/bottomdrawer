//
//  BottomViewController.swift
//  BottomDrawer
//
//  Created by Emanuele Beffa on 30/08/2018.
//  Copyright © 2018 Emanuele Beffa. All rights reserved.
//

import UIKit

class BottomViewController: UIViewController {
    
    var isOpened = false
    
    var lastY = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        
        setupSwipeLine()
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        view.addGestureRecognizer(gesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 150, width: view.frame.width, height: view.frame.height)
    }
    
    @objc func panGesture(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        if (sender.state == .began) {
            lastY = sender.location(in: view.window).y
        }
        
        if (view.frame.origin.y + translation.y < 100) {
            view.frame = CGRect(x: 0, y: 100 - pow(lastY - sender.location(in: view.window).y, 1 / 1.6), width: view.frame.width, height: view.frame.height)
        } else {
            view.frame = CGRect(x: 0, y: view.frame.minY + translation.y, width: view.frame.width, height: view.frame.height)
        }
        
        sender.setTranslation(CGPoint.zero, in: view)
        
        if (sender.state == .cancelled || sender.state == .ended) {
            if (isOpened) {
                if (view.frame.origin.y > 200) {
                    isOpened = false
                    UIView.animate(withDuration: 0.2) {
                        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 150, width: self.view.frame.width, height: self.view.frame.height)
                    }
                } else {
                    UIView.animate(withDuration: 0.2) {
                        self.view.frame = CGRect(x: 0, y: 100, width: self.view.frame.width, height: self.view.frame.height)
                    }
                }
            } else {
                if (view.frame.origin.y < UIScreen.main.bounds.height - 200) {
                    isOpened = true
                    UIView.animate(withDuration: 0.2) {
                        self.view.frame = CGRect(x: 0, y: 100, width: self.view.frame.width, height: self.view.frame.height)
                    }
                } else {
                    UIView.animate(withDuration: 0.2) {
                        self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 150, width: self.view.frame.width, height: self.view.frame.height)
                    }
                }
            }
        }
    }
    
    func setupSwipeLine() {
        let line = UIView(frame: CGRect(x: (UIScreen.main.bounds.width / 2) - 20, y: 8, width: 40, height: 6))
        line.isUserInteractionEnabled = true
        line.backgroundColor = .white
        line.layer.cornerRadius = 3
        line.clipsToBounds = true
        view.addSubview(line)
    }
}
