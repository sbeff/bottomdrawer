//
//  ViewController.swift
//  BottomDrawer
//
//  Created by Emanuele Beffa on 30/08/2018.
//  Copyright © 2018 Emanuele Beffa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBottomViewController()
    }
    
    func addBottomViewController() {
        let bottomViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomViewController") as! BottomViewController
        addChildViewController(bottomViewController)
        view.addSubview(bottomViewController.view)
        bottomViewController.didMove(toParentViewController: self)
        bottomViewController.view.frame = CGRect(x: 0, y: view.frame.maxY, width: view.frame.width, height: view.frame.height)
    }
}
